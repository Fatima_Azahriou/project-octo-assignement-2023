package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.ITransferService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"job.autorun.enabled=false"})
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class CompteRepositoryTest {
    @Autowired
    private ITransferService transferService;

    @Autowired
    private ICompteRepository compteRepository;
    @Test
    public void findOne() {
        Utilisateur utilisateur1=transferService.addUtilisateur("Fatima Azahriou1","Azahriou1",new Date(),"Fatima1","Female");

        Compte compte1=transferService.addCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

        assertEquals(compte1,compteRepository.getById(compte1.getId()));
    }
    @Test
    public void findAll() {
        Utilisateur utilisateur1=transferService.addUtilisateur("Fatima Azahriou2","Azahriou2",new Date(),"Fatima2","Female");
        Utilisateur utilisateur2=transferService.addUtilisateur("Fatima Azahriou3","Azahriou3",new Date(),"Fatima3","Female");

//        Utilisateur utilisateur2=transferService.addUtilisateur("AAAABBBB","BBBB",new Date(),"AAAA","Male");

        List<Compte> comptes=new ArrayList<>();

        comptes.add(transferService.addCompte("010000A000001001","RIB2",BigDecimal.valueOf(200000L),utilisateur1));

        comptes.add(transferService.addCompte("010000B025001001","RIB3",BigDecimal.valueOf(140000L),utilisateur2));
// j'ai ajouter +2 car on a deja ajouter 2 comptes dans la methode run de l'application
        assertEquals(comptes.size()+2,compteRepository.findAll().size());
    }

    @Test
    public void save() {
        Utilisateur utilisateur1=transferService.addUtilisateur("Amine","last1",new Date(),"first1","Male");

        Compte compte1=transferService.addCompte("010000A000001000","RIB1", BigDecimal.valueOf(200000L),utilisateur1);

        assertNotNull(compteRepository.findById(compte1.getId()));
        System.out.println(compte1);
    }
    @Test
    public void delete() {
        Utilisateur utilisateur1=transferService.addUtilisateur("user1","last1",new Date(),"first1","Male");

        Compte compte1=transferService.addCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

        compteRepository.delete(compte1);

        assertNull(compteRepository.findById(compte1.getId()).orElse(null));
    }


}