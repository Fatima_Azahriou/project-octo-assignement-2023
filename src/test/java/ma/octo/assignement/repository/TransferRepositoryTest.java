package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.ITransferService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;


import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class TransferRepositoryTest {

  @Autowired
  private ITransferRepository transferRepository;
  @Autowired
  private ITransferService transferService;

  @Test
  public void findOne() {
    Utilisateur utilisateur1=transferService.addUtilisateur("user1","last1",new Date(),"first1","Male");

    Utilisateur utilisateur2=transferService.addUtilisateur("user2","last2",new Date(),"first2","Female");

    Compte compte1=transferService.addCompte("010000A000001000","RIB1", BigDecimal.valueOf(200000L),utilisateur1);

    Compte compte2=transferService.addCompte("010000B025001000","RIB2",BigDecimal.valueOf(140000L),utilisateur2);

    Transfer transfer = transferService.addTransfer(BigDecimal.TEN,compte1,compte2,new Date(),"motifTest1");
    assertEquals(transferService.getTransferById(transfer.getId()),transfer);
  }

  @Test
  public void findAll() {
    Utilisateur utilisateur1=transferService.addUtilisateur("user3","last1",new Date(),"first1","Male");

    Utilisateur utilisateur2=transferService.addUtilisateur("user4","last2",new Date(),"first2","Female");

    Compte compte1=transferService.addCompte("010000A000001001","RIB3",BigDecimal.valueOf(200000L),utilisateur1);

    Compte compte2=transferService.addCompte("010000B025001001","RIB4",BigDecimal.valueOf(140000L),utilisateur2);
    List<Transfer> transfers = new ArrayList<>();


    transfers.add( transferService.addTransfer(BigDecimal.TEN,compte1,compte2,new Date(),"motif1"));
    transfers.add( transferService.addTransfer(BigDecimal.TEN,compte1,compte2,new Date(),"motif2"));

    List<Transfer> listTransfert = transferRepository.findAll();

// j'ai ajouter +1 car on a deja ajouter un transfert dans la methode run de l'application
    assertEquals(transfers.size()+1,listTransfert.size());

  }

  @Test
  public void saveTransfer() {
    Utilisateur utilisateur1=transferService.addUtilisateur("fatima","last1",new Date(),"first1","Male");

    Utilisateur utilisateur2=transferService.addUtilisateur("amine","last2",new Date(),"first2","Female");

    Compte compte1=transferService.addCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

    Compte compte2=transferService.addCompte("010000B025001000","RIB2",BigDecimal.valueOf(140000L),utilisateur2);

    Transfer transfer = transferService.addTransfer(BigDecimal.valueOf(1000),compte1,compte2,new Date(),"motifTest");
    transferRepository.save(transfer);

    assertNotNull(transferService.getTransferById(transfer.getId()));
    System.out.println(transfer);

  }

  @Test
  public void delete() {
    Utilisateur utilisateur1=transferService.addUtilisateur("user1","last1",new Date(),"first1","Male");

    Utilisateur utilisateur2=transferService.addUtilisateur("user2","last2",new Date(),"first2","Female");

    Compte compte1=transferService.addCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

    Compte compte2=transferService.addCompte("010000B025001000","RIB2",BigDecimal.valueOf(140000L),utilisateur2);

    Transfer transfer = transferService.addTransfer(BigDecimal.TEN,compte1,compte2,new Date(),"test");

    transferService.deleteTransfer(transfer.getId());

    assertNull(transferService.getTransferById(transfer.getId()));
  }
}