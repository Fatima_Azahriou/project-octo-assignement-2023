package ma.octo.assignement.service.Impl;

import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransferException;
import ma.octo.assignement.service.IDepositService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;


import static org.junit.jupiter.api.Assertions.assertThrows;
@RunWith(SpringRunner.class)
@SpringBootTest
class DepositServiceImplTest {

    @Autowired
    private IDepositService depositService;

    @Test
    void deposerPlusQue10000DH() {
        // depasser la valeur maximal
        assertThrows(DepositException.class,
                () -> depositService.createNewDeposit("Fatima Azahriou","RIB1",BigDecimal.valueOf(12000),"tt"));
    }

    @Test
    void deposerMontantEgaleZero() {
        // depasser la valeur maximal
        assertThrows(DepositException.class,
                () -> depositService.createNewDeposit("Fatima Azahriou","RIB1",BigDecimal.valueOf(0),"tt"));
    }
    @Test
    void deposerMontantInf10DH() {
        // valeur minimal non attient
        assertThrows(DepositException.class,
                () -> depositService.createNewDeposit("Fatima Azahriou","RIB1",BigDecimal.valueOf(9),"tt"));
    }
    @Test
    void deposerAvecMotifVide() {
        // motif vide
        assertThrows(DepositException.class,
                () -> depositService.createNewDeposit("Fatima Azahriou","RIB1",BigDecimal.valueOf(120),""));
    }

    @Test
    void compteRecepteurNonExistant() {
        // compte recepteur non existant
        assertThrows(CompteNonExistantException.class,
                () -> depositService.createNewDeposit("Fatima Azahriou", "RIBX", BigDecimal.valueOf(12000), "tt"));

    }

}
