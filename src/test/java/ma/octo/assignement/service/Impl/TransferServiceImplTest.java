package ma.octo.assignement.service.Impl;

import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransferException;
import ma.octo.assignement.service.ITransferService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;


import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@SpringBootTest
class TransferServiceImplTest {

    @Autowired
    private ITransferService transferService;

    @Test
    void transferPlusQue10000DH() {
        // depasser la valeur maximal
        assertThrows(TransferException.class,
                () -> transferService.createNewTransfer("010000A000001000","010000B025001000","tt", BigDecimal.valueOf(12000)));
    }

    @Test
    void transferMontantEgaleZero() {
        // depasser la valeur maximal
        assertThrows(TransferException.class,
                () -> transferService.createNewTransfer("010000A000001000","010000B025001000","tt", BigDecimal.valueOf(0)));
    }
    @Test
    void transferMontantInf10DH() {
        // valeur minimal non attient
        assertThrows(TransferException.class,
                () -> transferService.createNewTransfer("010000A000001000","010000B025001000","tt", BigDecimal.valueOf(9)));
    }
    @Test
    void transferAvecMotifVide() {
        // motif vide
        assertThrows(TransferException.class,
                () -> transferService.createNewTransfer("010000A000001000","010000B025001000","", BigDecimal.valueOf(2000)));
    }
    @Test
    void soldeInsuffisant() {
        // solde insuffisant
        assertThrows(SoldeDisponibleInsuffisantException.class,
                () -> transferService.createNewTransfer("010000A000001000","010000B025001000","aa", BigDecimal.valueOf(9500)));
    }
    @Test
    void compteEmetteurNonExistant() {
        // compte emetteur non existant
        assertThrows(CompteNonExistantException.class,
                () -> transferService.createNewTransfer("010000C000001000","010000B025001000","aa", BigDecimal.valueOf(9500)));
    }
    @Test
    void compteRecepteurNonExistant() {
        // compte recepteur non existant
        assertThrows(CompteNonExistantException.class,
                () -> transferService.createNewTransfer("010000A000001000","010000C025001000","aa", BigDecimal.valueOf(9500)));
    }



}
