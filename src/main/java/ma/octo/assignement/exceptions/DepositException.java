package ma.octo.assignement.exceptions;

public class DepositException extends Exception {

    private static final long serialVersionUID = 1L;

    public DepositException() {
    }

    public DepositException(String message) {
        super(message);
    }
}

