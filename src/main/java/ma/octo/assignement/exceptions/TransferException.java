package ma.octo.assignement.exceptions;

public class TransferException extends Exception {

  private static final long serialVersionUID = 1L;

  public TransferException() {
  }

  public TransferException(String message) {
    super(message);
  }
}
