package ma.octo.assignement.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class DepositDTO {
    private String nomPrenomEmetteur;
    private String rib;
    private BigDecimal montant;
    private Date date;
    private String motif;

}
