package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.repository.ICompteRepository;
import ma.octo.assignement.repository.IDepositRepository;
import ma.octo.assignement.repository.IUtilisateurRepository;
import ma.octo.assignement.repository.ITransferRepository;
import ma.octo.assignement.service.IDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class NiceBankApplication implements CommandLineRunner {
	@Autowired
	private ICompteRepository compteRepository;
	@Autowired
	private IUtilisateurRepository utilisateurRepository;
	@Autowired
	private ITransferRepository transferRepository;
	@Autowired
	private IDepositRepository depositRepository;

	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUserName("user1");
		utilisateur1.setLastName("last1");
		utilisateur1.setFirstName("first1");
		utilisateur1.setGender("Male");

		utilisateurRepository.save(utilisateur1);


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUserName("user2");
		utilisateur2.setLastName("last2");
		utilisateur2.setFirstName("first2");
		utilisateur2.setGender("Female");

		utilisateurRepository.save(utilisateur2);

		Compte compte1 = new Compte();
		compte1.setNumCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(9000L));
		compte1.setUtilisateur(utilisateur1);

		compteRepository.save(compte1);

		Compte compte2 = new Compte();
		compte2.setNumCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(8000L));
		compte2.setUtilisateur(utilisateur2);

		compteRepository.save(compte2);

		Transfer transfer = new Transfer();
		transfer.setMontantTransfer(BigDecimal.TEN);
		transfer.setCompteEmetteur(compte1);
		transfer.setCompteBeneficiaire(compte2);
		transfer.setDateExecution(new Date());
		transfer.setMotifTransfer("Assignment 2023");

		transferRepository.save(transfer);


		Deposit deposit = new Deposit();
		deposit.setMotifDeposit("deposit_1");
		deposit.setNomPrenomEmetteur("Fatima Azahriou");
		deposit.setMontant(BigDecimal.valueOf(1000));
		deposit.setCompteBeneficiaire(compte1);
		depositRepository.save(deposit);
	}
}
