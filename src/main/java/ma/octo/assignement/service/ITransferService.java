package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransferException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface ITransferService {
    void createNewTransfer(String numCompteEmetteur,String numCompteBeneficiaire,String motif,BigDecimal montant) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransferException;
    List<Transfer> getAllTransfers();
    Transfer getTransferById(Long id);
    List<Compte> getAllComptes();
    List<Utilisateur> getAllUtilisateurs();
    Transfer addTransfer(BigDecimal decimal, Compte beneficiaire, Compte emetteur, Date date, String motif);
    void deleteTransfer(Long id);
    Compte addCompte(String numCompte,String rib,BigDecimal montant,Utilisateur user);
    Utilisateur addUtilisateur(String username,String lastname,Date date,String firstName,String gender);
}

