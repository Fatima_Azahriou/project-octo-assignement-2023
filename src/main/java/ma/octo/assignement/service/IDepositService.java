package ma.octo.assignement.service;

import ma.octo.assignement.domain.*;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransferException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface IDepositService {


    void createNewDeposit(String nomPrenomEmetteur,String rib, BigDecimal montant,String motif) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransferException, DepositException;
    List<Deposit> getAllDeposits();
    Deposit addDeposit(String nomPrenomEmetteur, BigDecimal montant, Compte beneficiaire, Date date, String motif);
    Deposit getDepositById(Long id);

    void deleteDeposit(Long id);
}