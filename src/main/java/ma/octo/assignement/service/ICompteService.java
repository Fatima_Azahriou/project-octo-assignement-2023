package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;

import java.util.List;

public interface ICompteService {
    Compte findByNumCompte(String numCompte);
    Compte findByRib(String rib);

    List<Compte> findAll();

//    List<Compte> findByUser(Utilisateur utilisateur);
}
