package ma.octo.assignement.service.Impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.ICompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CompteServiceImpl implements ICompteService {
    @Autowired
    private final ICompteRepository compteRepository;

    public CompteServiceImpl(ICompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }

    @Override
    public Compte findByNumCompte(String numCompte) {
        return compteRepository.findByNumCompte(numCompte);
    }

    @Override
    public Compte findByRib(String rib) {
        return compteRepository.findByrib(rib);
    }

    @Override
    public List<Compte> findAll() {
        return compteRepository.findAll();
    }



}
