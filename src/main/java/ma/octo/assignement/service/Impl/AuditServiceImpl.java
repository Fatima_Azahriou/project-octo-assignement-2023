package ma.octo.assignement.service.Impl;

import ma.octo.assignement.domain.AuditDeposit;
import ma.octo.assignement.domain.AuditTransfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.IAuditDepositRepository;
import ma.octo.assignement.repository.IAuditTransferRepository;
import ma.octo.assignement.service.IAuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditServiceImpl implements IAuditService {
//    in this service layer we inject our repositories , DAL
    Logger LOGGER = LoggerFactory.getLogger(IAuditService.class);

    @Autowired
    private IAuditTransferRepository auditTransferRepository;
    @Autowired
    private IAuditDepositRepository auditDepositRepository;

    @Override
    public void auditTransfer(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.TRANSFER);
        LOGGER.info(message);
        AuditTransfer audit = new AuditTransfer();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage(message);
        auditTransferRepository.save(audit);
    }

    @Override
    public void auditDeposit(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.DEPOSIT);
        LOGGER.info(message);
        AuditDeposit audit = new AuditDeposit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage(message);
        auditDepositRepository.save(audit);
    }
}
