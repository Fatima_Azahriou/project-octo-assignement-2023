package ma.octo.assignement.service.Impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransferException;
import ma.octo.assignement.repository.ICompteRepository;
import ma.octo.assignement.repository.ITransferRepository;
import ma.octo.assignement.repository.IUtilisateurRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ITransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class TransferServiceImpl implements ITransferService {

    public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MINIMAL = 10;

    Logger logger = LoggerFactory.getLogger(TransferServiceImpl.class);
    @NonNull
    private ICompteRepository compteRepository;

    @NonNull
    private ITransferRepository transferRepository;

    @NonNull
    private IAuditService monService;

    @NonNull
    private IUtilisateurRepository utilisateurRepository;


    @Override
    public List<Transfer> getAllTransfers() {
        logger.info("Lister des transferts");
        return transferRepository.findAll();
    }

    @Override
    public Transfer getTransferById(Long id) {
        logger.info("Lister le transfert {}",id);
        return transferRepository.findById(id).orElse(null);
    }

    @Override
    public List<Compte> getAllComptes() {
        logger.info("Lister des comptes");
        return compteRepository.findAll();
    }

    @Override
    public List<Utilisateur> getAllUtilisateurs() {
        logger.info("Lister des utilisateurs");
        return utilisateurRepository.findAll();
    }

    @Override
    public Transfer addTransfer(BigDecimal decimal,Compte beneficiaire,Compte emetteur,Date date,String motif) {
        Transfer transfer = new Transfer();
        transfer.setMontantTransfer(decimal);
        transfer.setCompteBeneficiaire(beneficiaire);
        transfer.setCompteEmetteur(emetteur);
        transfer.setDateExecution(date);
        transfer.setMotifTransfer(motif);
        transferRepository.save(transfer);
        return transfer;
    }

    @Override
    public void deleteTransfer(Long id) {
        transferRepository.deleteById(id);
    }

    @Override
    public Compte addCompte(String numCompte, String rib, BigDecimal montant,Utilisateur user) {
        Compte compte = new Compte();
        compte.setNumCompte(numCompte);
        compte.setRib(rib);
        compte.setSolde(montant);
        compte.setUtilisateur(user);

        compteRepository.save(compte);
        return compte;
    }

    @Override
    public Utilisateur addUtilisateur(String userName, String lastName,Date date, String firstName, String gender) {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setUserName(userName);
        utilisateur.setLastName(lastName);
        utilisateur.setFirstName(firstName);
        utilisateur.setGender(gender);

        utilisateurRepository.save(utilisateur);
        return utilisateur;
    }
    @Override
    public void createNewTransfer(String numCompteEmetteur,String numCompteBeneficiaire,String motif,BigDecimal montant) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransferException {
        Compte compteEmetteur = compteRepository.findByNumCompte(numCompteEmetteur);
        Compte compteBeneficiaire = compteRepository.findByNumCompte(numCompteBeneficiaire);

        if (compteEmetteur == null ) {
            logger.info("Compte emetteur Non existant");
            throw new CompteNonExistantException("Compte emetteur Non existant");
        }
        if (compteBeneficiaire == null ) {
            logger.info("Compte récepteur Non existant");
            throw new CompteNonExistantException("Compte récepteur Non existant");
        }

        if (montant.intValue() == 0) {
            logger.info("Montant vide");
            throw new TransferException("Montant vide");
        } else if (montant.intValue() < MONTANT_MINIMAL) {
            logger.info("Montant minimal de transfer non atteint");
            throw new TransferException("Montant minimal de transfer non atteint");
        } else if (montant.intValue() > MONTANT_MAXIMAL) {
            logger.info("Montant maximal de transfer dépassé");
            throw new TransferException("Montant maximal de transfer dépassé");
        }
        if (motif.length() == 0) {
            logger.info("Motif vide");
            throw new TransferException("Motif vide");
        }

        if (compteEmetteur.getSolde().intValue() - montant.intValue() < 0) {
            logger.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant");
        }

// ajouter l'emetteur
        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(montant));
        compteRepository.save(compteEmetteur);
// ajouter le recepteur
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(montant));
        compteRepository.save(compteBeneficiaire);
// ajouter tout le transfert
        addTransfer(montant,compteBeneficiaire,compteEmetteur,new Date(),motif);

        monService.auditTransfer("Transfer depuis " + numCompteEmetteur + " vers " + numCompteBeneficiaire
                + " d'un montant de " + montant);
    }

}
