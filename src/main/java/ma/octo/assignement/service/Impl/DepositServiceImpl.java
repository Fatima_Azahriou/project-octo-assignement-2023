package ma.octo.assignement.service.Impl;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.*;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.exceptions.TransferException;
import ma.octo.assignement.repository.ICompteRepository;
import ma.octo.assignement.repository.IDepositRepository;
import ma.octo.assignement.repository.IUtilisateurRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.IDepositService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DepositServiceImpl implements IDepositService {


    public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MINIMAL = 10;

    Logger logger = LoggerFactory.getLogger(TransferServiceImpl.class);
    @Autowired
    private ICompteRepository compteRepository;
    @Autowired
    private IAuditService monService;
    @Autowired
    private IUtilisateurRepository utilisateurRepository;

    @Autowired
    private IDepositRepository depositRepository;

    public void createNewDeposit(String nomPrenomEmetteur,String rib,BigDecimal montant,String motif) throws CompteNonExistantException, DepositException, TransferException {

        Compte compte = compteRepository.findByrib(rib);

        if (compte == null) {
        logger.info("Compte réceppteur Non existant");
        throw new CompteNonExistantException("Vous essayer de deposer sur un compte récepteur Non existant");
    }

        if (montant.intValue() == 0) {
        logger.info("Deposit d'un Montant vide");
        throw new DepositException("Deposit d'un Montant vide");
    } else if (montant.intValue() < MONTANT_MINIMAL) {
        logger.info("Deposit d'un Montant minimal de Dépôt  non atteint");
        throw new DepositException("Deposit d'un Montant minimal de Dépôt  non atteint");
    } else if (montant.intValue() > MONTANT_MAXIMAL) {
        logger.info("Deposit d'un Montant maximal de Dépôt  dépassé");
        throw new DepositException("Deposit d'un Montant maximal de Dépôt dépassé");
    }
        if (motif.length() == 0) {
            logger.info("Deposit d'un avec un motif vide");
            throw new DepositException("Deposit d'un avec un motif vide");
        }


        compte.setSolde(compte.getSolde().add(new BigDecimal(montant.toString())));
        compteRepository.save(compte);
        addDeposit(nomPrenomEmetteur,montant,compteRepository.findByrib(compte.getRib()),new Date(),motif);

        monService.auditDeposit("Depot vers " + rib + " d'un montant de " + montant+" par "+nomPrenomEmetteur);
   }

    @Override
    public List<Deposit> getAllDeposits() {
        logger.info("Lister des deposits");
        return depositRepository.findAll();
    }


    public Deposit addDeposit(String nomPrenomEmetteur, BigDecimal montant, Compte beneficiaire, Date date, String motif) {
        Deposit deposit = new Deposit();
        deposit.setNomPrenomEmetteur(nomPrenomEmetteur);
        deposit.setDateExecution(date);
        deposit.setCompteBeneficiaire(beneficiaire);
        deposit.setMontant(montant);
        deposit.setMotifDeposit(motif);
        depositRepository.save(deposit);
        return deposit;
    }

    @Override
    public Deposit getDepositById(Long id) {
        logger.info("Lister le deposit {}",id);
        return depositRepository.findById(id).orElse(null);
    }
    @Override
    public void deleteDeposit(Long id) {
        depositRepository.deleteById(id);
    }


}
