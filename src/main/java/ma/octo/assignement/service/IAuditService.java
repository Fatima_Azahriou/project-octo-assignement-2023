package ma.octo.assignement.service;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


public interface IAuditService {

    public void auditTransfer(String message);

    public void auditDeposit(String message);
}
