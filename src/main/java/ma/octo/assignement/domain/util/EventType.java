package ma.octo.assignement.domain.util;

public enum EventType {

  TRANSFER("transfer"),
  DEPOSIT("Deposit d'argent");
// type de l'evenement
  private String type;

//  constructor
  EventType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
