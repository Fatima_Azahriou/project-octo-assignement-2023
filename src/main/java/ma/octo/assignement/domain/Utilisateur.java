package ma.octo.assignement.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "UTILISATEUR")
@Data
public class Utilisateur implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 20, nullable = false, unique = true)
  private String userName;

  @Column(length = 60, nullable = false)
  private String firstName;
  @Column(length = 60, nullable = false)
  private String lastName;

  @Column(length = 10, nullable = false)
  private String gender;


  @Temporal(TemporalType.DATE)
  private Date birthDate;

}
