package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDTO;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.service.ICompteService;

public class DepositMapper {
    private DepositMapper(){}


    private static ICompteService compteService;


    public static DepositDTO toDepositDto(Deposit deposit) {
        DepositDTO depositDTO;
        depositDTO = new DepositDTO();
        depositDTO.setNomPrenomEmetteur(deposit.getNomPrenomEmetteur());
        depositDTO.setRib(deposit.getCompteBeneficiaire().getRib());
        depositDTO.setMontant(deposit.getMontant());
        depositDTO.setDate(deposit.getDateExecution());
        return depositDTO;
    }
    public static Deposit toMonyDeposit(DepositDTO depositDTO) throws CompteNonExistantException {
        Compte compteBinificaire = compteService.findByRib(depositDTO.getRib());

        if (compteBinificaire == null) {
            throw new CompteNonExistantException("Copmte de recepteur non existant");
        }

        Deposit deposit = new Deposit();
        deposit.setCompteBeneficiaire(compteBinificaire);
        deposit.setNomPrenomEmetteur(depositDTO.getNomPrenomEmetteur());
        deposit.setMontant(depositDTO.getMontant());
        deposit.setDateExecution(depositDTO.getDate());

        return deposit;
    }

}
