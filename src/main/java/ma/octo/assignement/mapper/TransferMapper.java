package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.ITransferService;
//import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

// Without DTO, the presentation and the domain layers are tightly coupled.

public class TransferMapper {

    private static ICompteService compteService;
    private TransferMapper(){}

    public static TransferDto toTransfertDto(Transfer transfer) {
        TransferDto transferDto;
        transferDto = new TransferDto();
        transferDto.setNumCompteEmetteur(transfer.getCompteEmetteur().getNumCompte());
        transferDto.setNumCompteBeneficiaire(transfer.getCompteBeneficiaire().getNumCompte());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMotif(transfer.getMotifTransfer());
        transferDto.setMontant(transfer.getMontantTransfer());
        return transferDto;


    }
    public static Transfer toTransfer(TransferDto transferDto) throws CompteNonExistantException {

        Compte compteEmetteur = compteService.findByNumCompte(transferDto.getNumCompteEmetteur());
        Compte compteBeneficiaire = compteService.findByNumCompte(transferDto.getNumCompteBeneficiaire());
        if (compteEmetteur == null) {
            throw new CompteNonExistantException("Copmte d'emetteur non existant");
        }
        if (compteBeneficiaire == null) {
            throw new CompteNonExistantException("Copmte recepteur non existant");
        }

        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setMontantTransfer(transferDto.getMontant());

        return transfer;
    }

    }

