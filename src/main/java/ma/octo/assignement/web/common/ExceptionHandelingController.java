package ma.octo.assignement.web.common;

import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransferException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ExceptionHandelingController {

    @ExceptionHandler(SoldeDisponibleInsuffisantException.class)
    public ResponseEntity<String> handleSoldeDisponibleInsuffisantException(Exception ex, WebRequest request) {
        return new ResponseEntity<>("Pas de solde => pas de transfer", null, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
    }

    @ExceptionHandler(CompteNonExistantException.class)
    public ResponseEntity<String> handleCompteNonExistantException(Exception ex, WebRequest request) {
        return new ResponseEntity<>("Compte introuvable", null, HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler(DepositException.class)
    public ResponseEntity<String> handleDepositExeption(Exception ex, WebRequest request) {
        return new ResponseEntity<>("Vous devez choisir un montant supérieur à 10 DH et inférieure à 10000 DH et remplir le motif puis réssayer", null, HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler(TransferException.class)
    public ResponseEntity<String> handeleTransactionException(Exception ex, WebRequest request) {
        return new ResponseEntity<>("Vous devez choisir un montant supérieur à 10 DH et inférieure à 10000 DH et remplir le motif puis réssayer", null, HttpStatus.UNAUTHORIZED);
    }
}
