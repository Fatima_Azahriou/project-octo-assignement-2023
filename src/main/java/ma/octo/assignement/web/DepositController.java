package ma.octo.assignement.web;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDTO;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransferException;
import ma.octo.assignement.service.IDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/deposit/")
public class DepositController {

    @Autowired
    private IDepositService depositService;
    @PostMapping("/executerDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeposit(@RequestBody DepositDTO depositDTO) throws CompteNonExistantException, DepositException, TransferException, SoldeDisponibleInsuffisantException {
          depositService.createNewDeposit(depositDTO.getNomPrenomEmetteur(),depositDTO.getRib(),depositDTO.getMontant(),depositDTO.getMotif());
    }
    @GetMapping("/listDesDeposits")
    List<Deposit> loadAllDeposits() {
        return depositService.getAllDeposits();
    }

    @GetMapping("/delete/{id}")
    void deleteDeposit(@PathVariable Long id) {
        depositService.deleteDeposit(id);
    }
    @GetMapping("/getDepositById/{id}")
    void getDepositById(@PathVariable Long id) {
        depositService.getDepositById(id);
    }


}

