package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransferException;
//import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.service.ITransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/transfer/")
class TransferController {

// here we inject our service layer
    @Autowired
    private ITransferService transferService;

//    private TransferMapper transferMapper;

    @GetMapping("listDesTransferts")
    List<Transfer> loadAllTransferts() {
        return transferService.getAllTransfers();
    }

    @GetMapping("listOfAccounts")
    List<Compte> loadAllCompte() {
      return transferService.getAllComptes();
    }

    @GetMapping("lister_utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
       return transferService.getAllUtilisateurs();
    }
//L'annotation @RequestBody est applicable aux méthodes de gestion des contrôleurs Spring.
// Cette annotation indique que Spring doit désérialiser un corps de requête dans un objet.
// Cet objet est passé en tant que paramètre de méthode de gestionnaire.
    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransfer(@RequestBody TransferDto transferDto) throws TransferException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        transferService.createNewTransfer(transferDto.getNumCompteEmetteur(), transferDto.getNumCompteBeneficiaire(), transferDto.getMotif(),transferDto.getMontant());
    }

    @DeleteMapping("delete/{id}")
    void deleteTransfer(@PathVariable Long id){
        transferService.deleteTransfer(id);
    }

    @DeleteMapping("getTransferById/{id}")
    void getTransferById(@PathVariable Long id){
        transferService.getTransferById(id);
    }

}
