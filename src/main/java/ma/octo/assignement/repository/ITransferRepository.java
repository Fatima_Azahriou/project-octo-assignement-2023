package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import org.springframework.data.jpa.repository.JpaRepository;




//public interface JpaRepository<T,ID>
//extends PagingAndSortingRepository<T,ID>, QueryByExampleExecutor<T>
public interface ITransferRepository extends JpaRepository<Transfer, Long> {
}
